function initMap() {
    var slider = new Slider();
    var canvas = new Canvas();
    var timer = new Timer();
    var marker = new Marker(Map, Event, Reservation, Timer);
    var markercluster = new Markercluster();
    var reservation = new Reservation(Map, Marker, Event, Timer);
    var event = new Event(Map, Marker, Reservation, Timer);
    var map   = new Map(Map, Marker, Event, Reservation, Timer);
}
