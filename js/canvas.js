var canvas = document.getElementById("canvas")
var ctx = canvas.getContext("2d")
// var nom = document.getElementById("nom")
// var prenom = document.getElementById("prenom")

var SignIn = {
    dessin: false,

    // INITIALISE CANVAS
    initCanvas: function () {
        // Taille et couleur du trait de la signature
        ctx.strokeStyle = "red"
        ctx.lineWidth = 2

        SignIn.sourisEvent()
        SignIn.touchEvent()
        SignIn.dessiner()
    },


    // METHODE: EVENEMENT SOURIS
    // -------------------------
    sourisEvent: function () {

        // EVENT: Bouton de la souris enfoncé
        $("#canvas").on("mousedown", function (e) {
            SignIn.dessin = true
            ctx.beginPath()
            ctx.moveTo(e.offsetX, e.offsetY)

        })

        // var nom = document.getElementById("nom").value
        // var prenom = document.getElementById("prenom").value

        // EVENT: Déplacement de la souris
        $("#canvas").on("mousemove", function (e) {
            // Si le bouton est enfoncé, dessine
            if (SignIn.dessin === true && nom && prenom) {
                SignIn.dessiner(e.offsetX, e.offsetY)

                // localStorage.setItem(nom, "nom")
                // localStorage.setItem(prenom, "prenom")
                // location.reload()
                // Active le bouton "valider" et change la couleur
                $(".valider").prop("disabled", false)
                $(".valider").css("background-color", "red")
            }
        })

        // EVENT: Bouton de la souris relâché
        $("#canvas").on("mouseup", function (e) {
            SignIn.dessin = false
        })
    },


    // METHODE: GERE LES EVENEMENTS TACTILE SUR MOBILE
    // -----------------------------------------------
    touchEvent: function () {
        // EVENT: touché
        $("#canvas").on("touchstart", function (e) {
            var touchX = e.touches[0].pageX - e.touches[0].target.offsetLeft
            var touchY = e.touches[0].pageY - e.touches[0].target.offsetTop

            SignIn.dessin = true
            ctx.beginPath()
            ctx.moveTo(touchX, touchY)
            // Empêche le scrolling de l'écran
            e.preventDefault()
        })


        // EVENT: Déplacement du touché
        $("#canvas").on("touchmove", function (e) {
            var touchX = e.touches[0].pageX - e.touches[0].target.offsetLeft
            var touchY = e.touches[0].pageY - e.touches[0].target.offsetTop

            if (SignIn.dessin === true && nom && prenom) {
                SignIn.dessiner(touchX, touchY)

                localStorage.setItem("nom", nom)
                localStorage.setItem("prenom", prenom)
                location.reload()
                // Active le bouton "valider" et change la couleur
                $(".valider").prop("disabled", false)
                $(".valider").css("background-color", "red")
            }
            // Empêche le scrolling de l'écran
            e.preventDefault()
        })

        // EVENT: fin du touché
        $("#canvas").on("touchend", function (e) {
            SignIn.dessin = false
        })
    },
    
    // METHODE: EFFACER LA SIGNATURE
    // -----------------------------
    effacerSignature: function () {
        // Efface la signature
        ctx.clearRect(0, 0, 250, 125)
        // Désactive le bouton "valider" et change la couleur
        $(".valider").prop("disabled", true)
        $(".valider").css("background-color", "grey")
    },


    // METHODE: DESSINER
    // -----------------
    dessiner: function (x, y) {
        ctx.lineTo(x, y)
        ctx.stroke()
    }
}

SignIn.initCanvas()
