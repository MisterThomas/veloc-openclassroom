// OBJET: VelocMap
var VelocMap = {
    veloDispo: 0,
    placeDispo: 0,

    /*
        INITIALISE LA MAP
             initVeloc: function (Lat, Lng, api) {
                 Affiche la map
                 var iconBase = "http://veloc-p3.herokuapp.com/image/"
                 var map = new google.maps.Map(document.getElementById("map"), {
                     center: new google.maps.LatLng(Lat, Lng),
                     zoom: 13
                 })

                 RECUPERATION DES DONNEES JSON DE L'API
                 ajaxGet(api, function (reponse) {
                         var stations = JSON.parse(reponse)
                         Créé un tableau de markers
                         var markers = []


                         SI RESERVATION EN COURS, L'AFFICHE DANS LE FOOTER + localStorage "nom et prénom"
                         if (sessionStorage.station != null) {
                             $(document).ready(function () {
                                 $(".footer-text").text("Votre vélo à la station " + //sessionStorage.station
                                     + " est réservé pour :"
                                     + localStorage.getItem('prenom') + " "
                                     + localStorage.getItem("nom")
                                 )

                                 $("#footer *:not(.timer)").fadeIn("Slow")
                                 var timeInterval = setInterval(VelocMap.compteur, 1000)

                                 EVENT: Annuler la réservation
                                 $(".annuler").on("click", function () {
                                     VelocMap.annulerReservation()
                                 })
                             })
                         }*/

    // POUR CHAQUE STATION
    stations.forEach(function (station) {

    // AFFICHE LE MARQUEUR SUR LA MAP
    var marker = new google.maps.Marker({
        position: station.position,
        title: station.name,
        status: station.status,
        map: map,
        availbikes: station.available_bikes,
        availbikestands: station.available_bike_stands,
        bikestands: station.bike_stands,
    })

    // Definition de la couleur du marqueur en fonction du statut "OUVERT"/"FERME" de la station
    if (marker.status !== "OPEN") {
        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png')
    } else {
        if (marker.status === "OPEN" && !marker.availbikes) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')
        } else {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png')
        }
    }


    if (marker.availbikes === 0 && marker.status === 0) {
        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png')
    }

    marker.setAnimation(null)
    marker.addListener('click', function () {

        VelocMap.majInfos(station.name, station.available_bikes, station.available_bike_stands)

        if (marker.getAnimation() !== null) {
            marker.setAnimation(null)

            $(".infos-station *:visible:not(h2)").fadeOut("slow", function () {
                $(".nom-station").text("Sélectionnez une station").fadeIn("slow")
            })
        }
        else {
            markers.forEach(function (marker) {
                marker.setAnimation(null)
            })

            marker.setAnimation(google.maps.Animation.BOUNCE)
            VelocMap.afficherInfos(station.name, station.address)
        }
        $(".reservation").fadeOut("slow")
    })
    markers.push(marker)
})

var markerCluster = new MarkerClusterer(map, markers, {
    imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m"
})
},

// METHODE: AFFICHAGE DES INFOS STATIONS ET DU CADRE RESERVATION
// -------------------------------------------------------------
afficherInfos: function (nomStation, adresseStation) {
    // Cache tous les éléments visible dans infos-station sauf le titre
    $(".infos-station *:visible:not(h2)").fadeOut("slow", function () {
        // Remplit les détails de la station
        $(".nom-station").text(nomStation)
        $(".adresse-station").text(adresseStation)
        $(".place-libre").text("Il y a " + VelocMap.placeDispo + " places libres")
        $(".velo-dispo").text("Il y a " + VelocMap.veloDispo + " vélo(s) disponible(s)")

        // SI IL Y A DES VELOS DISPO ET SI UN VELO N'EST PAS DEJA RESERVE DANS CETTE STATION
        if ((VelocMap.veloDispo > 0) && (nomStation != sessionStorage.station)) {
            // Affiche toutes les infos + bouton "réserver"
            $(".infos-station *").fadeIn("slow")

            // Affiche le cadre reservation quand on clique sur "réserver"
            $(".reserver").on("click", function () {
                VelocMap.scrollTo($("#reservation"))
                $(".reservation").fadeIn("slow")
                $(".reservation hr").fadeIn("slow")
            })
        }
        else {
            $(".infos-station *:not(.reserver)").fadeIn("slow")
        }
    })
}
,

// METHODE: MISE A JOUR DES INFOS STATION
// --------------------------------------
majInfos: function (station, velo, place) {

    if (station == sessionStorage.station) {
        VelocMap.veloDispo = (velo - 1)
        VelocMap.placeDispo = (place + 1)
    }
    else {
        VelocMap.veloDispo = velo
        VelocMap.placeDispo = place
    }
}
,

// METHODE: SCROLL VERS UNE TARGET
// -------------------------------
scrollTo: function (target) {
    $("html, body").stop().animate({scrollTop: target.offset().top}, "slow")
}
}


function initMap() {
    VelocMap.initMap(45.750, 4.850, "https://api.jcdecaux.com/vls/v1/stations?contract=Lyon&apiKey=caf5a23676519f91740f243c1393fed29c9064f5")
}
