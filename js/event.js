// Scroll vers infos-station quand on clique sur le marker
                    VelocMap.scrollTo($("#formulaire"))

// EVENT: Valider la réservation
                    $(".valider").on("click", function () {
                        // Validation de la réservation
                        VelocMap.validerReservation(station.name)
                        // Mise à jour des infos
                        VelocMap.majInfos(station.name, station.available_bikes, station.available_bike_stands)
                        // Affiche le nombre de vélos et de places actualisés
                        $(".velo-dispo").text("Il y a " + (VelocMap.veloDispo) + " vélo(s) disponible(s)").fadeIn("slow")
                        $(".place-libre").text("Il y a " + (VelocMap.placeDispo) + " places libres").fadeIn("slow")
                        // Effacer la signature
                        VelocMap.effacerSignature()
                    })
 // EVENT: Effacer la signature
                    $(".effacer").on("click", function () {
                        VelocMap.effacerSignature()
                    })

     // EVENT: Annuler la réservation
                    $(".annuler").on("click", function () {
                        // Annule la réservation
                        VelocMap.annulerReservation()
                        // Mise à jour des infos
                        VelocMap.majInfos(station.name, station.available_bikes, station.available_bike_stands)
                        // Affiche les infos
                        VelocMap.afficherInfos(station.name, station.adress)
                    })