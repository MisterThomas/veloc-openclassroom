var $boutonLeft = $("#left")
var $boutonRight = $("#right")
var $etape = $(".etape")
var $body = $("body")

// Objet Slider
var Slider = {
    slideIndex: 1,
    sliderAuto: null,


    // Méthode: initialise le Slider
    initSlider: function () {
        Slider.afficherSlider()
        Slider.clickBouton()
        Slider.eventClavier()
        Slider.pause()
        // Slider.nextSlider()
        // Slider.prevSlider()

        // Démarrage du slider automatique
        Slider.automatic()
    },

    automatic: function () {
        Slider.sliderAuto = setInterval(function () {
            Slider.slideIndex++
            Slider.afficherSlider(Slider.slideIndex)
        }, 5000)
    },

    // Méthode: affichage du slider
    afficherSlider: function () {
        if (Slider.slideIndex === 5) {
            Slider.slideIndex = 1
        }
        // Index = 1 s'il dépasse le nombre d'éléments du slider
        // console.log($etape)
        // if (slider.slideindex > $etape.length) {
        //     Slider.slideIndex = 1
        // }
        // index = dernier élément du slider si il dépasse le premier élément
        // if (prevSlider < 1) {
        //     Slider.slideIndex = $etape.length
        // }
        // N'affiche aucun élément du slider
        $etape.hide()
        // Affiche l'élément du slider voulu
        $etape.eq(Slider.slideIndex - 1).fadeIn("slow")
    },
    // Méthode: changer de slide
    nextSlider: function () {
        // Arrête le slider auto
        clearInterval(Slider.sliderAuto)
        if (Slider.slideIndex === 4)
            Slider.afficherSlider(Slider.slideIndex = 1)
        else
            Slider.afficherSlider(Slider.slideIndex += 1)
    },

    // Méthode: changer de slide
    prevSlider: function () {
        clearInterval(Slider.sliderAuto)
        if (Slider.slideIndex === 1) {
            Slider.slideIndex = 5
        }
        Slider.afficherSlider(Slider.slideIndex -= 1)
    },
    clickBouton: function () {
        // Event du clic sur le bouton droit
        $boutonRight.on("click", function () {
            Slider.nextSlider()
        })
        // Event du clic sur le bouton gauche
        $boutonLeft.on("click", function () {
            Slider.prevSlider()
        })
    },
    eventClavier: function () {
        $("body").on("keyup", function (e) {
            sens = e.keyCode
            if (sens === 37) {
                Slider.prevSlider()
            } else if (sens === 39) {
                Slider.nextSlider()
            }
        })
    },

    pause: function () {
        var slider = document.getElementsByClassName('etape')
        var playing = true
        var pauseButton = document.getElementById('pause')

        function pauseSlidershow() {
            playing = false
            clearInterval(Slider.sliderAuto)
            var el = document.querySelector(".fa-pause-circle")
            el.classList.add("fa-play-circle")
            el.classList.remove("fa-pause-circle")
        }

        function playSlidershow() {
            playing = true
            var el = document.querySelector(".fa-play-circle")
            el.classList.add("fa-pause-circle")
            el.classList.remove("fa-play-circle")
            Slider.automatic()
        }

        pauseButton.onclick = function () {
            if (playing) {
                pauseSlidershow()
            }
            else {
                playSlidershow()
            }
        }
    }
}


Slider.initSlider()
