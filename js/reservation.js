// METHODE: VALIDATION DE LA RESERVATION
// -------------------------------------
validerReservation: function (station) {


    var nom = document.getElementById("nom")
    var prenom = document.getElementById("prenom")
    // Efface la station en mémoire locoStorage.clear();
    sessionStorage.clear()
    // Enregistre le nom de la station de la réservation
    $(".reserver").onclick = function () {
        var nom = nom.value
        var prenom = prenom.value

        //console.log(nom);
        //console.log(prenom);

        if (nom && prenom) {
            localStorage.setItem(nom, "nom")
            localStorage.setItem(prenom, "prenom")
            location.reload()
        }
    }         // if prend en compte nom & prenom de input dans le front.
    //variable pour sotcker getitem puis les rappeller plus tard dans footer.

    for (let i = 0; i < localStorage.lenght; i++) {
        var nom = localStorage.nom(i)
        var nom = localStorage.getItem("nom")
        var prenom = localStorage.getItem("prenom")
    }

    sessionStorage.setItem("station", station)
    //locoStorage.setItem(".nom");
    // Initialise la date de fin de réservation
    var dateReservation = Date.parse(new Date())
    var deadline = new Date(dateReservation + 20 * 60 * 1000)
    // Enregistre date de fin de la réservation
    sessionStorage.setItem("date", deadline)

    // Scroll vers footer quand on valide la réservation
    VelocMap.scrollTo($("#footer"))

    //Cache le bouton et le panneau de réservation.
    $(".reserver").fadeOut("slow")
    $(".reservation").fadeOut("slow")
    localStorage.setItem("nom", nom.value)
    localStorage.setItem("prenom", prenom.value)
    console.log(sessionStorage)

    // Affiche la réservation et le timer + localStorage "nom et prénom"
    $("#footer *:not(h3, .annuler, .timer)").fadeOut("slow", function () {
        $(".footer-text").text("Votre vélo à la station "
            + sessionStorage.station + " est réservé pour : "
            + localStorage.getItem("prenom") + " "
            + localStorage.getItem("nom"))

        $("#footer *").fadeIn("slow")
    })

    // Lance le compte à rebours de la réservation
    var timeInterval = setInterval(VelocMap.compteur, 1000)
}
,



// METHODE: COMPTEUR DE LA RESERVATION
// -----------------------------------
compteur: function () {
    // t = temps restant jusqu'à la deadline en ms
    var temps = Date.parse(sessionStorage.date) - Date.parse(new Date())
    // Conversion de t en secondes et minutes
    var secondes = Math.floor((temps / 1000) % 60)
    var minutes = Math.floor((temps / 1000 / 60) % 60)
    // Affichage du compteur
    $(".minutes").text(minutes + " min")
    $(".secondes").text(("0" + secondes + " s").slice(-4))

    // ANNULE LA RESERVATION A LA FIN DU COMPTE A REBOURS
    if (temps <= 0) {
        VelocMap.annulerReservation()
        // Affiche un message "Réservation terminée"
        $(".infos-station *:visible:not(h2)").fadeOut("slow", function () {
            $(".nom-station").text("Réservation terminée").fadeIn("slow")
        })
    }
}
,

// EVENT: Annuler la réservation
$(".annuler").on("click", function () {
    // Annule la réservation
    VelocMap.annulerReservation()
    // Mise à jour des infos
    VelocMap.majInfos(station.name, station.available_bikes, station.available_bike_stands)
    // Affiche les infos
    VelocMap.afficherInfos(station.name, station.adress)
})
