// INITIALISE LA MAP

var Map = {
    initMap: function (Lat, Lng, api) {
        // Affiche la map
        var iconBase = "http://veloc-p3.herokuapp.com/image/"
        var map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng(Lat, Lng),
            zoom: 13
        })


        // RECUPERATION DES DONNEES JSON DE L'API
        ajaxGet(api, function (reponse) {
            var stations = JSON.parse(reponse)
            // Créé un tableau de markers
            var markers = []


            // SI RESERVATION EN COURS, L'AFFICHE DANS LE FOOTER + localStorage "nom et prénom"
            if (sessionStorage.station != null) {
                $(document).ready(function () {
                    $(".footer-text").text("Votre vélo à la station " + sessionStorage.station
                        + " est réservé pour :"
                        + localStorage.getItem('prenom') + " "
                        + localStorage.getItem("nom")
                    )

                    $("#footer *:not(.timer)").fadeIn("Slow")
                    var timeInterval = setInterval(VelocMap.compteur, 1000)

                    // EVENT: Annuler la réservation
                    $(".annuler").on("click", function () {
                        VelocMap.annulerReservation()
                    })
                })
            }
        })
    }
}
